\documentclass[]{beamer}

\usetheme{Singapore}

\usepackage{tabularx}

\makeatletter
\setbeamertemplate{footline}
{
    \hspace{0.01\paperwidth}
    \begin{beamercolorbox}[wd=0.2\paperwidth]{}
        \insertshortauthor\hspace{1ex}(\insertshortinstitute)
    \end{beamercolorbox}
    \begin{beamercolorbox}[wd=0.55\paperwidth]{}
        \centering\insertshorttitle
    \end{beamercolorbox}
    \begin{beamercolorbox}[wd=0.2\paperwidth]{}
        \hfill\insertframenumber/\inserttotalframenumber
    \end{beamercolorbox}
    \vspace{1pt}
}
\makeatother

\newcommand*{\tocfootline}{
\makeatletter
\setbeamertemplate{footline}
{
    \hspace{0.01\paperwidth}
    \begin{beamercolorbox}[wd=0.2\paperwidth]{}
        \insertshortauthor\hspace{1ex}(\insertshortinstitute)
    \end{beamercolorbox}
    \begin{beamercolorbox}[wd=0.55\paperwidth]{}
        \centering\insertshorttitle
    \end{beamercolorbox}
    \vspace{1pt}
}
\makeatother
}

\usepackage{caption}
\usepackage{multirow}

\usepackage[
    sorting=none,
    bibstyle=authoryear,
    citestyle=authoryearsquare,
    maxbibnames=10,
    maxcitenames=10,
    mincitenames=1,
    backend=bibtex,
]{biblatex}
\addbibresource{bibliography.bib}
\addbibresource{custom.bib}
\renewcommand*{\bibfont}{\tiny}

\usepackage{tikz}
\usetikzlibrary{
    arrows.meta,
    backgrounds,
    fit,
    positioning,
}
\tikzset{
    eq/.style = {
        fill=blue!20,
        text width=12em,
        minimum height=2em,
        text centered,
        rounded corners,
        line width=0,
    },
    bound/.style = {
        fill=green!20,
        text width=13em,
        rounded corners,
        line width=0,
    },
    arrow/.style = {
        -{>[length=2mm, width=3mm]},
    },
    highlight/.style = {
        fill=\light{#1},
        rounded corners,
    },
    text label/.style = {
        text width=10em,
        align=center,
    }
}

\newcommand{\light}[1]{#1!20}

\newcommand{\pcite}[1]{\textcolor{gray}{\tiny\parencite{#1}}}

\newcommand{\osection}[1]{
    \section{#1}

    \begingroup
    \tocfootline
    \begin{frame}[noframenumbering]{Outline}
        \tableofcontents[currentsection]
    \end{frame}
    \endgroup
}

\graphicspath{{./img/}}

\usefonttheme[onlymath]{serif}

\def\talktitle{Two-loop five-point amplitudes in massless QCD with finite fields}

\title{\talktitle}
\author{Ryan Moodie}
\institute[Turin]{Turin University}
\date{High Precision for Hard Processes \\\vspace{1em} 20 Sep 2022}
\titlegraphic{
    \begin{tikzpicture}[x=7em]
        \def\l{3em}
        \node at (0,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{infn_logo}};
        \node at (1,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{torino_logo}};
        \node at (2,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{erc_logo}};
    \end{tikzpicture}
}

\begin{document}

\begingroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
    \titlepage
\end{frame}
\endgroup

\begingroup
\tocfootline
\begin{frame}[noframenumbering]{Outline}
    \tableofcontents
\end{frame}
\endgroup

\osection{Introduction}

\begin{frame}{Precision frontier}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Better understand properties of SM
                \item Indirect probe of new physics by small deviations
                \item LHC demanding increasing precision
                \item Theory predictions at 1\%
                \item Requires $\ge$ NNLO QCD
                    \begin{align*}
                        \alpha_s\approx&\,0.1\\
                        \mathrm{d}\sigma=&\,\mathrm{d}\sigma_{\mathrm{LO}}+\alpha_s\,\mathrm{d}\sigma_{\mathrm{NLO}} \\
                        &+ \alpha_s^2\,\mathrm{d}\sigma_{\mathrm{NNLO}}+\ldots
                    \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{jets_v1}
                \captionsetup{labelformat=empty}
                \caption{\pcite{!collaboration:2114784}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\osection{Processes}

\begin{frame}{Trijet production}
    \begin{itemize}
        \item NNLO leading-colour $pp\to jjj$
            \begin{columns}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=7em]{jjj}
                    \end{figure}
                \end{column}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=14em]{r32_pT1_2}
                        \captionsetup{labelformat=empty}
                        \caption{\pcite{Czakon:2021mjy}}
                    \end{figure}
                \end{column}
            \end{columns}
        \item Probe fundamental parameters of SM
        \item $R_{3/2} \to \alpha_s$
        \item Significant theory uncertainty reduction
        \begin{itemize}
            \item[] \pcite{Abreu:2021oya}
            \item[] \pcite{Czakon:2021mjy}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Diphoton-plus-jet production via gluon fusion}
    \begin{itemize}
        \item NLO full-colour $gg\to g\gamma\gamma$ (N$^3$LO $pp\to g\gamma\gamma$)
            \begin{columns}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=7em]{jjy}
                    \end{figure}
                \end{column}
                \begin{column}{0.5\textwidth}
                    \begin{figure}
                        \includegraphics[width=14em]{Mg1g2_2}
                    \end{figure}
                \end{column}
            \end{columns}
        \item[] \pcite{Badger:2021imn}
            \vspace{-4pt}
        \item[] \pcite{Badger:2021ohm}
        \item Important background for measuring Higgs properties
        \item Significant NLO corrections, enhances NNLO $pp\to g\gamma\gamma$
        \begin{itemize}
            \item[] \pcite{Chawdhry:2021hkp}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{$pp\to g\gamma\gamma$}
    \begin{tikzpicture}
        \node (main) {
        \includegraphics[width=0.9\textwidth]{perturbative}
        };
        \node at (1,-2.25) [highlight={green}] (LO) {LO};
        \node [right = 1em of LO.east, highlight={yellow}] (NLO) {NLO};
        \node [right = 1em of NLO.east, highlight={orange}] (NNLO) {NNLO};
        \node [right = 1em of NNLO.east, highlight={red}] (N3LO) {N$^3$LO};
        \node [below = 1em of NNLO.south, highlight={blue}] (LILO) {LO};
        \node [below = 1em of N3LO.south, highlight={cyan}] (LINLO) {NLO};
        \node [left = 1em of LILO.west] (LI) {Loop induced:};
    \end{tikzpicture}
\end{frame}

\osection{Computation}

\begin{frame}{Five-point two-loop computation}
    \begin{itemize}
        \item Major theoretical challenge
            \begin{itemize}
                \item New methods
            \end{itemize}
    \end{itemize}
    \begin{itemize}
        \item Reconstruct over finite fields
            \begin{itemize}
                \item[] \pcite{Peraro:2019svx}
            \end{itemize}
        \item Pentagon function basis
            \begin{itemize}
                \item[] \pcite{Gehrmann:2018yef}
                \item[] \pcite{Chicherin:2020oor}
                \item[] \pcite{Chicherin:2021dyp}
            \end{itemize}
        \item Fast and stable implementations in \texttt{NJet}
            \begin{itemize}
                \item[] \pcite{!njet}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Method overview}
    \begin{figure}
        \begin{center}
            \scalebox{0.8}{
                \begin{tikzpicture}[node distance=4em]
                    \def\ps{\boldsymbol{p}};
                    \def\f{\boldsymbol{f}\,(\ps)};
                    \def\mon{\textrm{mon}};
                    \def\adj{-5em};

                    \node (diagrams) [eq] {$\sum_i \mathrm{diagram}_i(\ps, \epsilon)$};
                    \node (integrands) [eq, right of=diagrams, xshift=14em] {$\sum_i \boldsymbol{a}_i(\ps, \epsilon) \cdot \boldsymbol{F}_i(\ps)$};
                    \node (scalars) [eq, below of=integrands] {$\sum_i b_i(\ps, \epsilon) \,\mathcal{F}_i(\ps)$};
                    \node (masters) [eq, below of=scalars] {$\sum_i c_i(\ps, \epsilon) \,\mathrm{MI}_i(\ps)$};
                    \node (specials) [eq, below of=masters] {$\sum_i d_i(\ps, \epsilon) \,\mon_i(\f)$};
                    \node (remainders) [eq, below of=specials] {$\sum_i e_i(\ps) \,\mon_i(\f)$};

                    \begin{scope}[on background layer]
                        \node [bound, fit=(integrands)(scalars)(masters)(specials)(remainders)] (ff) {};
                    \end{scope}

                    \path [arrow]
                    (diagrams.east) edge node [below] {Algebra} (integrands.west)
                    ([xshift=\adj] integrands.south) edge node [right] {Global integrand map} ([xshift=\adj] scalars.north)
                    ([xshift=\adj] scalars.south) edge node [right] {IBP identities} ([xshift=\adj] masters.north)
                    ([xshift=\adj] masters.south) edge node [right] {Special function basis} ([xshift=\adj] specials.north)
                    ([xshift=\adj] specials.south) edge node [right] {Subtract poles} ([xshift=\adj] remainders.north)
                    ;

                    \node (hels) [above of=diagrams, yshift=-4ex] {Helicity amplitudes};
                    \node (qgraf) [below of=diagrams, yshift=2ex, text label] {\texttt{qgraf}\\\pcite{Nogueira:1991ex}};
                    \node (form) [below of=qgraf, yshift=2ex, text label] {\texttt{FORM}\\\pcite{Ruijl:2017dtg}};
                    \node [left of=scalars, xshift=-12ex, text label] {\texttt{LiteRED}\\\pcite{Lee:2012cn}};
                    \node [left of=masters, xshift=-12ex, text label] {Laporta\\\pcite{Laporta:2000dsw}};
                    \node (pfuncs) [left of=specials, xshift=-19ex, text label] {\texttt{PentagonFunctions++}\\\pcite{Chicherin:2021dyp}};
                    \node (dipole) [left of=remainders, xshift=-16ex, text label] {Dipole scheme\\\pcite{Catani:1996vz}};

                    \node (label) [rounded corners, minimum height=2em, minimum width=7em, fill=\light{green}, line width=0, above of=integrands] {Finite fields};

                    \node [left of=label, xshift=-6ex, text label] {\texttt{FiniteFlow}\\\pcite{Peraro:2019svx}};
                    \path (label) edge [color=\light{green}, line width=0.5em] (integrands);

                \end{tikzpicture}
            }
        \end{center}
    \end{figure}
\end{frame}

\osection{Finite fields}

\begin{frame}{Finite field arithmetic}
    \begin{itemize}
        \item Set of $n\in\mathbb{P}^p$ non-negative integers
            $$ \mathbb{F}_{n}=\{0,\ldots,n-1\} $$
        \item Arithmetic operations modulo $n$
            \begin{table}
            \begin{tabular}{ccc}
                \textcolor{blue!70!black}{\textbullet} Addition &
                \textcolor{blue!70!black}{\textbullet} Multiplication &
                \textcolor{blue!70!black}{\textbullet} Inverses
            \end{tabular}
            \end{table}
        \item Modular multiplicative inverse $x= a^{-1} \mod n$
            \begin{align*}
                a x &= 1 \mod n  & a&\neq0
            \end{align*}
        \item One-to-many map $\mathbb{Q}\to\mathbb{F}_{n}$
            $$ \frac{a}{b} \,\to\, a\, b^{-1} \mod n $$

    \end{itemize}
\end{frame}

\begin{frame}{Numeric representation for computation}
    \begin{description}
        \item[\texttt{float}] Catastrophic cancellation
        \item[\texttt{int}] Overflow
        \item[$\mathbb{Q}$] Slow
        \item[$\mathbb{F}_n$]
            \begin{itemize}
                \item No precision loss ($n$ large)
                \item Fast: fixed size integer
                \item Recover $\mathbb{F}_{n}\to\mathbb{Q}$
                \item Chinese Remainder Theorem: $\{\mathbb{F}_{n}\}\to\mathbb{F}_{\prod n}$
            \end{itemize}
    \end{description}
    \begin{itemize}
        \item Amplitudes
            \begin{itemize}
                \item Large intermediate expressions
                \item Bypass with numerical evaluation over $\mathbb{F}_n$
                \item Reconstruct analytic expression
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Rational on-shell parametrisation}
    \begin{itemize}
        \item Momentum twistor variables
            $$\langle i j \rangle, [ij] \to x_i$$
            $$Z = \begin{pmatrix} \lambda_1 & \cdots & \lambda_n \\ \mu_1(\boldsymbol{\tilde\lambda}) & \cdots & \mu_n(\boldsymbol{\tilde\lambda}) \end{pmatrix}$$
            \item Rational functions
                \begin{itemize}
                    \item $p_i(\boldsymbol{x}), s_{ij}(\boldsymbol{x}), \mathrm{tr}_5(\boldsymbol{x}), \ldots$
                \end{itemize}
            \item $\boldsymbol{x}$ unconstrained
                \begin{itemize}
                    \item On-shell
                    \item Momentum conserving
                \end{itemize}
    \end{itemize}
\end{frame}

\osection{Reconstruction}

\begin{frame}{Reconstruction}
    \begin{itemize}
        \item Amplitude components
            $$
            F(\boldsymbol{x}) = \sum_{i} r_i(\boldsymbol{x}) \, \text{mon}_i(f)
            $$
        \item Have numerical algorithm for $r_i$
        \item \texttt{FiniteFlow} \pcite{Peraro:2019svx}
            \begin{figure}
                \vspace{1ex}
                \includegraphics[width=0.5\textwidth]{black-box}
                \vspace{1ex}
            \end{figure}
        \item Reconstruct expression from sufficient evaluations
        \item Strategies to optimise and compactify
    \end{itemize}
\end{frame}

\begin{frame}{Linear relations in the coefficients}
    \begin{itemize}
        \item Linearise the $r_i$
            $$
            \{ r_i \}_{i\in S} \to \{ r_i \}_{i\in T \subseteq S}
            $$
        \item Numerically solve:
            $$
            \sum_i a_i \, r_i(\boldsymbol{x})  = 0
            $$
            and choose lowest degrees
        \item Ans\"atze, eg permutations of $5g$ $r_i$ for $3g2\gamma$:
            $$
            \sum_i a_i \, r_i(\boldsymbol{x}) + \sum_j b_j \, e_j(\boldsymbol{x}) = 0
            $$
    \end{itemize}
\end{frame}

\begin{frame}{Matching factors}
    \begin{itemize}
        \item Coefficient ansatz
            $$
            r(\boldsymbol{x}) = \frac{n(\boldsymbol{x})}{\prod_{k} {\ell_k}^{e_k}(\boldsymbol{x})}
            \qquad
            \ell_k\in\text{pentagon alphabet}
            $$
        \item Determine $e_k$ by reconstructing $r$ on univariate slice
            $$
            \boldsymbol{x} = \boldsymbol{c}_0 + \boldsymbol{c}_1 t \qquad \rightsquigarrow \qquad r(t)
            $$
            and matching RHS
        \item Fix denominators
            $$
            \{ r_i \} \to \{ n_i \}
            $$
        \item[] \pcite{Abreu:2018zmy}
    \end{itemize}
\end{frame}

\begin{frame}{Univariate partial fraction decomposition}
    \begin{itemize}
        \item Example in $y$
            $$
            \frac{N(x,y)}{x^2y^2(x^2+y^2)} =\frac{q_1(x)}{y}+\frac{q_2(x)+q_3(x)y}{y^2}+\frac{q_4(x)+q_5(x)y}{x^2+y^2}
            $$
            \vspace{-2ex}
            \begin{itemize}
                \item Only need to know $y$ degree of $N$
                \item Choose $y$ by studying one-loop
            \end{itemize}
        \item Reconstruct $r_i$ directly in decomposed form
            $$
            \{ r_i(\overline{\boldsymbol{x}},y) \} \to \{ q_i(\overline{\boldsymbol{x}}) \}
            $$
            \vspace{-3ex}
            \begin{itemize}
                \item Reduce variables by one
                \item Lower degrees
            \end{itemize}
        \item Simplifies reconstruction
            \begin{itemize}
                \item $\times 10$ point numerical evaluation time (linear fit in $\{q_i\}$ over $y$)
                \item $\times \frac{1}{100}$ samples required
            \end{itemize}
    \end{itemize}
\end{frame}

\osection{Performance}

\begin{frame}{Implementation}
    \begin{itemize}
        \item Finite remainders coded up in \texttt{C++} as analytic expressions
        \item Construct partial amplitudes as
            \begin{equation*}
                F^{h} = r^h_{i}(\boldsymbol{x}) \, M^h_{ij} \,\, f^h_j
            \end{equation*}
            \begin{tabular}{lll}
                $f^h_j$ & special function monomials & global \\
                $M^h_{ij}$ & rational sparse matrices & partials \\
                $r^h_{i}$ & independent rational coefficients & helicities \\
                $\boldsymbol{x}$ & momentum twistor variables & global \\
            \end{tabular}
            \vspace{1ex}
        \item Independent helicities permuted to all mostly-plus
            \begin{itemize}
                \item Mostly-minus: ${r_{i}}^* \, M_{ij} \,\, P(f_j)$
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Evaluation strategy}
    \begin{figure}
        \includegraphics[width=\textwidth]{eval_hor}
    \end{figure}
\end{frame}

\begin{frame}{Timing}
    \begin{table}
        \begin{tabular}{| l | r r | r r |}
            \hline
            \multicolumn{1}{|c|}{\multirow{2}*{Channel}} & \multicolumn{2}{c|}{\texttt{f64/f64}} & \multicolumn{2}{c|}{Evaluation strategy} \\
            \cline{2-5}
            & Time (s) & $f$~(\%) & Time (s) & $f$~(\%) \\
            \hline
            $gg \to ggg$                         & 1.39 & 69 & 1.89 & 77 \\
            $gg \to \bar{q}qg$                   & 1.35 & 91 & 1.37 & 91 \\
            $qg \to qgg$                         & 1.34 & 92 & 1.57 & 93 \\
            $q\bar{q} \to ggg$                   & 1.34 & 93 & 1.38 & 93 \\
            $\bar{q}Q \to Q\bar{q}g$             & 1.14 & 99 & 1.16 & 99 \\
            $\bar{q}\bar{Q} \to \bar{q}\bar{Q}g$ & 1.36 & 99 & 1.39 & 99 \\
            $\bar{q}g \to \bar{q}Q\bar{Q}$       & 1.36 & 99 & 1.39 & 99 \\
            $\bar{q}q \to Q\bar{Q}g$             & 1.14 & 99 & 1.14 & 99 \\
            $\bar{q}g \to \bar{q}q\bar{q}$       & 1.84 & 99 & 1.90 & 99 \\
            $\bar{q}\bar{q} \to \bar{q}\bar{q}g$ & 1.82 & 99 & 1.94 & 99 \\
            $\bar{q}q \to q\bar{q}g$             & 1.71 & 99 & 1.77 & 99 \\
            \hline
            $gg\to\gamma\gamma g$ $*$   & 9    & 99 & 26   & 99 \\
            \hline
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}{Stability}
    \begin{tikzpicture}
        \node (main) {
            \includegraphics[width=\textwidth]{stability_all}
        };
        \node (asterisk) at (2.5, 2.75) {$*$};
        \node (inset) at (3.75, -1.75) {
            \includegraphics[width=0.3\textwidth]{euler}
        };
    \end{tikzpicture}
\end{frame}

\begin{frame}{IR performance}
    \begin{figure}
        \includegraphics[width=\textwidth]{collinear}
    \end{figure}
\end{frame}

\begin{frame}{Phenomenology}
    \begin{figure}
        \includegraphics[width=0.49\textwidth]{pt_g1g2_2}
        \includegraphics[width=0.49\textwidth]{dy_2}
    \end{figure}
    \begin{center}
        $gg\to g\gamma\gamma$
    \end{center}
\end{frame}

\osection{Conclusion}

\begin{frame}{Conclusion}{\talktitle}
    \begin{itemize}
        \item Huge success of technical advancements in precision QCD
        \begin{itemize}
            \item Numerical evaluation over finite fields
            \item Bypass intermediate complexity
            \item Reconstruct compact analytic forms
        \end{itemize}
        \vspace{1em}
        \item Efficient and stable evaluation over physical scattering region
        \begin{itemize}
            \item $pp\to jjj$ (LC)
            \item $gg\to g\gamma\gamma$
        \end{itemize}
        \vspace{1em}
        \item Driving pheno predictions towards 1\%
    \end{itemize}
\end{frame}

\appendix
\begingroup
\tocfootline
\begin{frame}[allowframebreaks,noframenumbering]{References}
    \printbibliography
\end{frame}
\endgroup

\end{document}

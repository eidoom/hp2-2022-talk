# [hp2-2022-talk](https://gitlab.com/eidoom/hp2-2022-talk)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7759875.svg)](https://doi.org/10.5281/zenodo.7759875)

[Live here](https://eidoom.gitlab.io/hp2-2022-talk/slides.pdf)

[Event](https://conference.ippp.dur.ac.uk/event/1100/) [contribution](https://conference.ippp.dur.ac.uk/event/1100/contributions/5770/)

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
